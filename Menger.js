/**
 * This program creates the Menger Sponge with a big cube and subcubes. 
 * It controls interactive key pressed events to accordingly change the Sponge.
 * Draws the Sponge to the canvas, and allows rotation of the Sponge.  
 */

var currCube = [];		// array that stores all cubes in current big cube
var lastCube = [];		// array that stores all cubes in previous big cube

// Setup function for canvas and first big cube
function setup() {
	createCanvas(700, 700, WEBGL);			// canvas of size 700*700	
	currCube.push(new Cube(0, 0, 0, 200));	// current & previous cubes should contain the first cube
	lastCube = currCube;
}

// Function responsible for key pressed activities
function keyPressed() {
	// press shift key (or equivalent) for one more iteration 
	if (keyCode == SHIFT) {
		lastCube = currCube; 	// store the current cube as last cube
		var newCube = []; 		// new big cube
		// for every cube in current cube, subdivide and add to new cube created above
		for (var c = 0; c < currCube.length; c++) {
			newCube = newCube.concat(currCube[c].subdivide());	
		}
		currCube = newCube; 	// set current cube as new big cube
	}
	// press alt key (or equivalent) to go back 1 step
	else if (keyCode == ALT) {
		currCube = lastCube;
	}
	// press control key (or equivalent) to go back to beginning
	else if (keyCode == CONTROL) {
		lastCube = currCube;
		currCube = [];
		currCube.push(new Cube(0, 0, 0, 200));
	}
}


// Constructor for a cube object that takes 4 arguments about position and size. 
// When key is pressed, subdivide function of cube object will be called to
// iterate the cube (subdivide and remove forbidden cubes), rendering a new cube.
// Display function allows drawing each cube on the canvas using push & pop matrix.
function Cube(x, y, z, size) {
	this.x = x;				// x, y, z coordinates of the cube
	this.y = y;
	this.z = z;
	this.size = size;		// size for a cube

	// Recursion function that helps generate deeper level of Menger Sponge
	this.subdivide = function() {
		var cubes = [];		// array to store new cubes
		// For each x, y, z coordinates from 0-2 (as digits can be 0, 1, 2)
		for (var i = 0; i <= 2; i++) {
			for (var j = 0; j <= 2; j++) {
				for (var k = 0; k <= 2; k++) {
					// if coordinate is one of forbidden string u_i, it should not be included
					if((i === 1 && j === 1) || (i === 1 && k === 1) || (j === 1 && k === 1)) continue;
					// else if coordinate does not violate the rule, it's added to the array
					// we decrease the size of cube by 1/3 and update x,y,z coordinates
					else {
						var newSize = this.size/3; 
						var c = new Cube(this.x + i*newSize, this.y + j*newSize, this.z + k*newSize, newSize);
						cubes.push(c);
					}
				}
			}
		}
		return cubes;
	}
	
	// draw() function below can call this function for each cube to show them on canvas
	// we move (translate) the object to the specified coordinates and draw the cube by
	// calling the primitive box() function 
	this.drawCube = function() {
		push();
		translate(this.x, this.y, this.z);
		box(this.size);
		pop();
	}
}

// Function for drawing cubes onto the canvas
function draw() {
	// Rotate x and y axes according to mouse drag
	if(mouseIsPressed) {
		rotateX(mouseY * 0.02);
		rotateY(mouseX * 0.02);
	}

	// Display every cube
	for (var c = 0; c < currCube.length; c++) {
		currCube[c].drawCube();
	}
}