When you open html file, you will see a cube. You can drag your mouse around for rotation of the sponge. You can use 3 keys to manipulate the cube.
1. To iterate/ generate subcubes, press Shift key
2. To go back a step, press Alt key
3. To go back to starting state, press Control key
